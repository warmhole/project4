/*
 * CSc103 Project 4: Triangles
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 *Kelly Lu: 30 min
 *Hyun Jun Nam: 2hrs
 *Shateesh Bhugwansing: 2 hrs.
 *Franklin Figueroa: 2hrs
 */

#include "triangles.h" // import the prototypes for our triangle class.
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;

// note the "triangle::" part.  We need to specify the function's
// FULL name to avoid confusion.  Else, the compiler will think we
// are just defining a new function called "perimeter"
unsigned long triangle::perimeter() {
	return s1+s2+s3;
}

unsigned long triangle::area() {
	// TODO: write this function.
	// Note: why is it okay to return an integer here?  Recall that
	// all of our triangles have integer sides, and are right triangles...
	unsigned long sides[3] = {s1,s2,s3}; //put sides in array
	sort(sides,sides+3); //sort array->sides[0]<=sides[1]<=sides[2]

	//assume only called for right trianges
	//area of triangel=(h*b)/2
	int a = (sides[0]*sides[1])/2;
	return a;
}

void triangle::print() {
	cout << "[" << s1 << "," << s2 << "," << s3 << "]";
}

bool congruent(triangle t1, triangle t2) {
	unsigned long t1sides[3] = {t1.s1,t1.s2,t1.s3};
    sort(t1sides, t1sides+3);
    unsigned long t2sides[3] = {t2.s1,t2.s2,t2.s3};
    sort(t2sides, t2sides+3);
	//sort sides

	//compare sides: t1[0]==t2[0] && t1[1]==t[2] && t1[3]==t2[3] return true
	if(t1sides[0] == t2sides[0] && t1sides[1] == t2sides[1] && t1sides[2] == t2sides[2]){
    	return true;
    }

    else{
	return false;
}

bool similar(triangle t1, triangle t2) {
    unsigned long t1sides[3] = {t1.s1,t1.s2,t1.s3};
    sort(t1sides, t1sides+3);
    unsigned long t2sides[3] = {t2.s1,t2.s2,t2.s3};
    sort(t2sides, t2sides+3);
    //checks if sides are the same
    if((t1sides[0]*t2sides[1])==(t1sides[1]*t2sides[0])&&(t1sides[2]*t2sides[1])==(t1sides[1]*t2sides[2])){
        return true;
    }
	else{
	return false;}
}


vector<triangle> findRightTriangles(unsigned long l, unsigned long h) {
	// TODO: find all the right triangles with integer sides, subject to the perimeter bigger than l and less than h
	vector<triangle> retval; // storage for return value.
	for(unsigned long a = 1; a <= h-2; a++){
		for(unsigned long b = 1; b <= h-2; b++){
			for(unsigned long c = 1; c <= h-2; c++){
				if(((a*a)+(b*b)==(c*c)) && (a<=b) && (a<=c) && (b<=c) && (l<=(a+b+c)) && ((a+b+c)<=h)){
					triangle temp(a,b,c);
					retval.push_back(temp); }
					}}}


	//1)find integers
	//2)distinguish right triangles
	//3)bounded by input perimeters: l<=p<=h

	return retval;
}

